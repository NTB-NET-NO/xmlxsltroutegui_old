Imports System.Data.OleDb
Imports System.Configuration.ConfigurationSettings

Public Class FormXpath
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Initier()
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents CheckedListBox1 As System.Windows.Forms.CheckedListBox
    Friend WithEvents CheckedListBox2 As System.Windows.Forms.CheckedListBox
    Friend WithEvents CheckedListBox3 As System.Windows.Forms.CheckedListBox
    Friend WithEvents CheckedListBox4 As System.Windows.Forms.CheckedListBox
    Friend WithEvents CheckedListBox5 As System.Windows.Forms.CheckedListBox
    Friend WithEvents CheckedListBox6 As System.Windows.Forms.CheckedListBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton4 As System.Windows.Forms.RadioButton
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents ButtonLagre As System.Windows.Forms.Button
    Friend WithEvents ButtonCancel As System.Windows.Forms.Button
    Friend WithEvents ButtonOK As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox4 As System.Windows.Forms.CheckBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox5 As System.Windows.Forms.CheckBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents CheckedListBox8 As System.Windows.Forms.CheckedListBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents CheckedListBox7 As System.Windows.Forms.CheckedListBox
    Friend WithEvents CheckedListBox9 As System.Windows.Forms.CheckedListBox
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents CheckedListBox10 As System.Windows.Forms.CheckedListBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.CheckedListBox1 = New System.Windows.Forms.CheckedListBox()
        Me.CheckedListBox2 = New System.Windows.Forms.CheckedListBox()
        Me.CheckedListBox3 = New System.Windows.Forms.CheckedListBox()
        Me.CheckedListBox4 = New System.Windows.Forms.CheckedListBox()
        Me.CheckedListBox5 = New System.Windows.Forms.CheckedListBox()
        Me.CheckedListBox6 = New System.Windows.Forms.CheckedListBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ButtonLagre = New System.Windows.Forms.Button()
        Me.ButtonCancel = New System.Windows.Forms.Button()
        Me.ButtonOK = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.RadioButton4 = New System.Windows.Forms.RadioButton()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.CheckBox5 = New System.Windows.Forms.CheckBox()
        Me.CheckBox4 = New System.Windows.Forms.CheckBox()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.CheckedListBox9 = New System.Windows.Forms.CheckedListBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.CheckedListBox8 = New System.Windows.Forms.CheckedListBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.CheckedListBox7 = New System.Windows.Forms.CheckedListBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.CheckedListBox10 = New System.Windows.Forms.CheckedListBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.SuspendLayout()
        '
        'CheckedListBox1
        '
        Me.CheckedListBox1.CheckOnClick = True
        Me.CheckedListBox1.Location = New System.Drawing.Point(432, 24)
        Me.CheckedListBox1.Name = "CheckedListBox1"
        Me.CheckedListBox1.Size = New System.Drawing.Size(184, 169)
        Me.CheckedListBox1.TabIndex = 0
        Me.CheckedListBox1.ThreeDCheckBoxes = True
        '
        'CheckedListBox2
        '
        Me.CheckedListBox2.CheckOnClick = True
        Me.CheckedListBox2.Location = New System.Drawing.Point(432, 216)
        Me.CheckedListBox2.Name = "CheckedListBox2"
        Me.CheckedListBox2.Size = New System.Drawing.Size(184, 154)
        Me.CheckedListBox2.TabIndex = 1
        Me.CheckedListBox2.ThreeDCheckBoxes = True
        '
        'CheckedListBox3
        '
        Me.CheckedListBox3.CheckOnClick = True
        Me.CheckedListBox3.Location = New System.Drawing.Point(632, 24)
        Me.CheckedListBox3.Name = "CheckedListBox3"
        Me.CheckedListBox3.Size = New System.Drawing.Size(184, 349)
        Me.CheckedListBox3.TabIndex = 2
        Me.CheckedListBox3.ThreeDCheckBoxes = True
        '
        'CheckedListBox4
        '
        Me.CheckedListBox4.CheckOnClick = True
        Me.CheckedListBox4.Location = New System.Drawing.Point(832, 24)
        Me.CheckedListBox4.Name = "CheckedListBox4"
        Me.CheckedListBox4.Size = New System.Drawing.Size(184, 349)
        Me.CheckedListBox4.TabIndex = 3
        Me.CheckedListBox4.ThreeDCheckBoxes = True
        '
        'CheckedListBox5
        '
        Me.CheckedListBox5.CheckOnClick = True
        Me.CheckedListBox5.Location = New System.Drawing.Point(232, 192)
        Me.CheckedListBox5.Name = "CheckedListBox5"
        Me.CheckedListBox5.Size = New System.Drawing.Size(184, 184)
        Me.CheckedListBox5.TabIndex = 4
        Me.CheckedListBox5.ThreeDCheckBoxes = True
        '
        'CheckedListBox6
        '
        Me.CheckedListBox6.CheckOnClick = True
        Me.CheckedListBox6.Location = New System.Drawing.Point(232, 400)
        Me.CheckedListBox6.Name = "CheckedListBox6"
        Me.CheckedListBox6.Size = New System.Drawing.Size(184, 349)
        Me.CheckedListBox6.TabIndex = 5
        Me.CheckedListBox6.ThreeDCheckBoxes = True
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(432, 576)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(368, 72)
        Me.TextBox1.TabIndex = 6
        Me.TextBox1.Text = ""
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(432, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(184, 16)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Stoffgruppe"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(432, 200)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(184, 16)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Undergruppe"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(632, 8)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(184, 16)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Kategori"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(832, 8)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(184, 16)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Underkategori"
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(232, 176)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(184, 16)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Omr�de"
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(232, 384)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(184, 16)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Fylke"
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(432, 560)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(104, 16)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Ords�k"
        '
        'ButtonLagre
        '
        Me.ButtonLagre.Location = New System.Drawing.Point(504, 712)
        Me.ButtonLagre.Name = "ButtonLagre"
        Me.ButtonLagre.TabIndex = 14
        Me.ButtonLagre.Text = "Lagre"
        '
        'ButtonCancel
        '
        Me.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ButtonCancel.Location = New System.Drawing.Point(712, 712)
        Me.ButtonCancel.Name = "ButtonCancel"
        Me.ButtonCancel.TabIndex = 15
        Me.ButtonCancel.Text = "Avbryt"
        '
        'ButtonOK
        '
        Me.ButtonOK.Location = New System.Drawing.Point(608, 712)
        Me.ButtonOK.Name = "ButtonOK"
        Me.ButtonOK.TabIndex = 16
        Me.ButtonOK.Text = "OK"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.AddRange(New System.Windows.Forms.Control() {Me.RadioButton2, Me.RadioButton1})
        Me.GroupBox1.Location = New System.Drawing.Point(616, 504)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(184, 48)
        Me.GroupBox1.TabIndex = 17
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Forhold mellom bokser"
        '
        'RadioButton2
        '
        Me.RadioButton2.Location = New System.Drawing.Point(96, 16)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(80, 24)
        Me.RadioButton2.TabIndex = 1
        Me.RadioButton2.Text = "AND-s�k"
        '
        'RadioButton1
        '
        Me.RadioButton1.Location = New System.Drawing.Point(16, 16)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(72, 24)
        Me.RadioButton1.TabIndex = 0
        Me.RadioButton1.Text = "OR-S�k"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.AddRange(New System.Windows.Forms.Control() {Me.RadioButton3, Me.RadioButton4})
        Me.GroupBox2.Location = New System.Drawing.Point(432, 656)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(176, 48)
        Me.GroupBox2.TabIndex = 18
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Ord kombinert med kategorier"
        '
        'RadioButton3
        '
        Me.RadioButton3.Location = New System.Drawing.Point(88, 16)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(72, 24)
        Me.RadioButton3.TabIndex = 1
        Me.RadioButton3.Text = "AND-s�k"
        '
        'RadioButton4
        '
        Me.RadioButton4.Location = New System.Drawing.Point(8, 16)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(64, 24)
        Me.RadioButton4.TabIndex = 0
        Me.RadioButton4.Text = "OR-S�k"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(616, 384)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox2.Size = New System.Drawing.Size(472, 112)
        Me.TextBox2.TabIndex = 23
        Me.TextBox2.Text = ""
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(808, 504)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(232, 23)
        Me.Button4.TabIndex = 25
        Me.Button4.Text = "Test av &XPath"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.AddRange(New System.Windows.Forms.Control() {Me.CheckBox5, Me.CheckBox4, Me.CheckBox3, Me.CheckBox2, Me.CheckBox1})
        Me.GroupBox3.Location = New System.Drawing.Point(432, 392)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(168, 160)
        Me.GroupBox3.TabIndex = 26
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Andre tilbud"
        '
        'CheckBox5
        '
        Me.CheckBox5.Location = New System.Drawing.Point(16, 112)
        Me.CheckBox5.Name = "CheckBox5"
        Me.CheckBox5.Size = New System.Drawing.Size(128, 24)
        Me.CheckBox5.TabIndex = 4
        Me.CheckBox5.Text = "Meldinger med foto"
        '
        'CheckBox4
        '
        Me.CheckBox4.Location = New System.Drawing.Point(16, 88)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(128, 24)
        Me.CheckBox4.TabIndex = 3
        Me.CheckBox4.Text = "Direkte-melding"
        '
        'CheckBox3
        '
        Me.CheckBox3.Location = New System.Drawing.Point(16, 64)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.TabIndex = 2
        Me.CheckBox3.Text = "HAST"
        '
        'CheckBox2
        '
        Me.CheckBox2.Enabled = False
        Me.CheckBox2.Location = New System.Drawing.Point(16, 40)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.TabIndex = 1
        Me.CheckBox2.Text = "SMS"
        '
        'CheckBox1
        '
        Me.CheckBox1.Location = New System.Drawing.Point(16, 16)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.TabIndex = 0
        Me.CheckBox1.Text = "Lyd-nyheter"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(1048, 504)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(40, 20)
        Me.TextBox3.TabIndex = 27
        Me.TextBox3.Text = ""
        Me.TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TabControl1
        '
        Me.TabControl1.Controls.AddRange(New System.Windows.Forms.Control() {Me.TabPage1, Me.TabPage2})
        Me.TabControl1.Location = New System.Drawing.Point(8, 8)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(216, 240)
        Me.TabControl1.TabIndex = 28
        '
        'TabPage1
        '
        Me.TabPage1.Controls.AddRange(New System.Windows.Forms.Control() {Me.CheckedListBox9})
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(208, 214)
        Me.TabPage1.TabIndex = 1
        Me.TabPage1.Text = "Produkter"
        '
        'CheckedListBox9
        '
        Me.CheckedListBox9.CheckOnClick = True
        Me.CheckedListBox9.Items.AddRange(New Object() {"Nyhetstjenesten", "NTB Direkte", "Kultur og Underholdning", "Kultur Direkte", "Feature", "Varsling/SMS", "Lyd-nyheter", "Tips", "Personalia", "Dagen i Dag", "Pressemeldinger", "Nyhetskalender"})
        Me.CheckedListBox9.Location = New System.Drawing.Point(12, 8)
        Me.CheckedListBox9.Name = "CheckedListBox9"
        Me.CheckedListBox9.Size = New System.Drawing.Size(184, 199)
        Me.CheckedListBox9.TabIndex = 25
        Me.CheckedListBox9.ThreeDCheckBoxes = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.AddRange(New System.Windows.Forms.Control() {Me.Label9, Me.CheckedListBox8})
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(208, 222)
        Me.TabPage2.TabIndex = 0
        Me.TabPage2.Text = "Satellittkoder"
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(8, 8)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(184, 16)
        Me.Label9.TabIndex = 26
        Me.Label9.Text = "Satellittkoder"
        '
        'CheckedListBox8
        '
        Me.CheckedListBox8.CheckOnClick = True
        Me.CheckedListBox8.Items.AddRange(New Object() {"ALL", "UTE", "OKO", "KUL", "FKT", "VUR", "TOT", "DDO", "ODT", "DID", "PRS", "KOR", "RTX", "IPS", "ORI"})
        Me.CheckedListBox8.Location = New System.Drawing.Point(8, 24)
        Me.CheckedListBox8.Name = "CheckedListBox8"
        Me.CheckedListBox8.Size = New System.Drawing.Size(184, 184)
        Me.CheckedListBox8.TabIndex = 25
        Me.CheckedListBox8.ThreeDCheckBoxes = True
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(232, 8)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(184, 16)
        Me.Label8.TabIndex = 24
        Me.Label8.Text = "Utmappe"
        '
        'CheckedListBox7
        '
        Me.CheckedListBox7.CheckOnClick = True
        Me.CheckedListBox7.Items.AddRange(New Object() {"Ut-Satellitt", "Ut-Direkte", "Ut-Kultur", "Ut-SMS", "Ut-Portal", "Ut-Feature", "Ut-Diverse", "Ut-KulturDirekte", "Ut-Internt"})
        Me.CheckedListBox7.Location = New System.Drawing.Point(232, 24)
        Me.CheckedListBox7.Name = "CheckedListBox7"
        Me.CheckedListBox7.Size = New System.Drawing.Size(184, 139)
        Me.CheckedListBox7.TabIndex = 23
        Me.CheckedListBox7.ThreeDCheckBoxes = True
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(24, 264)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(184, 16)
        Me.Label10.TabIndex = 30
        Me.Label10.Text = "Meldingstype"
        '
        'CheckedListBox10
        '
        Me.CheckedListBox10.CheckOnClick = True
        Me.CheckedListBox10.Items.AddRange(New Object() {"DagenIDag-melding", "DiDDagensSitat-melding", "DiDFloFj�re-melding", "DiDF�dselsdag-melding", "DiDHendt-melding", "DiDKjentfolk-melding", "DiDMerkedag-melding", "DiDNavnedag-melding", "DiDSolOppNed-melding", "Faktaboks", "Feature-melding", "Feature-melding", "Feature-Ukemeny", "FeatureTeaser-melding", "Hastemelding", "Il-melding", "Kultur-melding", "Logg-melding", "Meny-melding", "Notis-melding", "Omtale-melding", "OversiktNotis-melding", "PlussDagsMeny-melding", "PlussUkeMeny-melding", "Priv-melding", "PRM-melding", "PRM-PrivRed", "ReportersOnTheRoad", "SMS-melding", "Sportsresultater", "Standard nyhetsmelding", "Stortinget", "Tips-melding", "Ungdoms-melding", "UngdomsTeaser-melding"})
        Me.CheckedListBox10.Location = New System.Drawing.Point(24, 280)
        Me.CheckedListBox10.Name = "CheckedListBox10"
        Me.CheckedListBox10.Size = New System.Drawing.Size(184, 469)
        Me.CheckedListBox10.Sorted = True
        Me.CheckedListBox10.TabIndex = 29
        Me.CheckedListBox10.ThreeDCheckBoxes = True
        '
        'FormXpath
        '
        Me.AcceptButton = Me.ButtonOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.CancelButton = Me.ButtonCancel
        Me.ClientSize = New System.Drawing.Size(1096, 766)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.Label10, Me.CheckedListBox10, Me.TabControl1, Me.TextBox3, Me.GroupBox3, Me.Button4, Me.TextBox2, Me.GroupBox2, Me.GroupBox1, Me.ButtonOK, Me.ButtonCancel, Me.ButtonLagre, Me.Label7, Me.Label6, Me.Label5, Me.Label4, Me.Label3, Me.Label2, Me.Label1, Me.TextBox1, Me.CheckedListBox6, Me.CheckedListBox5, Me.CheckedListBox4, Me.CheckedListBox3, Me.CheckedListBox2, Me.CheckedListBox1, Me.Label8, Me.CheckedListBox7})
        Me.Name = "FormXpath"
        Me.Text = "FormXpath"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private dsBitnames As DataSet
    Private sqlConnectionString As String = AppSettings("SqlConnectionString")

    Private Sub Initier()
        FillDataSets()
        FillListBoxes()
    End Sub

    Private Sub SelectAll(ByRef box As CheckedListBox, ByVal index As Integer, ByVal newValue As CheckState, ByVal curentValue As CheckState)
        Static setting As Boolean
        If setting Then Exit Sub

        If index = 0 Then
            Dim i As Integer
            For i = 1 To box.Items.Count - 1
                box.SetItemChecked(i, newValue)
            Next
        Else
            setting = True
            box.SetItemChecked(0, False)
            setting = False
        End If

    End Sub

    Private Sub CheckedListBox1_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles CheckedListBox1.ItemCheck
        SelectAll(CheckedListBox1, e.Index, e.NewValue, e.CurrentValue)
    End Sub

    Private Sub CheckedListBox2_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles CheckedListBox2.ItemCheck
        SelectAll(CheckedListBox2, e.Index, e.NewValue, e.CurrentValue)
    End Sub

    Private Sub CheckedListBox3_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles CheckedListBox3.ItemCheck
        SelectAll(CheckedListBox3, e.Index, e.NewValue, e.CurrentValue)
    End Sub

    Private Sub CheckedListBox3_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckedListBox3.SelectedValueChanged
        SetSubcats()
    End Sub

    Private Sub CheckedListBox4_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles CheckedListBox4.ItemCheck
        SelectAll(CheckedListBox4, e.Index, e.NewValue, e.CurrentValue)
    End Sub

    Private Sub CheckedListBox5_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles CheckedListBox5.ItemCheck
        SelectAll(CheckedListBox5, e.Index, e.NewValue, e.CurrentValue)
    End Sub

    Private Sub CheckedListBox6_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles CheckedListBox6.ItemCheck
        SelectAll(CheckedListBox6, e.Index, e.NewValue, e.CurrentValue)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        MakeXpath()
    End Sub

    Private Sub CheckedListBox1_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckedListBox1.Leave
        CheckedListBox1.ClearSelected()
    End Sub
    Private Sub CheckedListBox2_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckedListBox2.Leave
        CheckedListBox2.ClearSelected()
    End Sub

    Private Sub CheckedListBox3_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckedListBox3.Leave
        CheckedListBox3.ClearSelected()
    End Sub


    Private Sub CheckedListBox4_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckedListBox4.Leave
        CheckedListBox4.ClearSelected()
    End Sub
    Private Sub CheckedListBox5_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckedListBox5.Leave
        CheckedListBox5.ClearSelected()
    End Sub
    Private Sub CheckedListBox6_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckedListBox6.Leave
        CheckedListBox6.ClearSelected()
    End Sub
    Private Sub CheckedListBox7_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs)
        CheckedListBox7.ClearSelected()
    End Sub
    Private Sub CheckedListBox8_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs)
        CheckedListBox8.ClearSelected()
    End Sub



    Private Sub MakeXpath()
        Me.TextBox2.Clear()
        Dim itm As String
        Dim xPath As String
        Dim strOr As String
        Dim strUnion As String
        strUnion = ""

        Dim queryCount As Integer = 0

        If CheckedListBox7.CheckedItems.Count > 0 Then
            queryCount += 1
            '/nitf/head/meta[@name='ntb-folder'][@content='Ut-Satellitt' or @content='Ut-Direkte']
            xPath = "/nitf/head/meta[@name='ntb-folder']["
            For Each itm In CheckedListBox7.CheckedItems
                xPath &= strOr & "@content='" & itm & "'"
                strOr = " or "
            Next
            xPath &= "]"
            strUnion = " | "
        End If

        If CheckedListBox1.CheckedItems.Count = 0 Then
        ElseIf Not CheckedListBox1.GetItemChecked(0) Then
            ' /nitf/head/tobject[(@tobject.type='Innenriks' or @tobject.type='Utenriks') 
            queryCount += 1
            xPath &= strUnion
            xPath &= "/nitf/head/tobject[("
            strOr = ""
            For Each itm In CheckedListBox1.CheckedItems
                xPath &= strOr & "@tobject.type='" & itm & "'"
                strOr = " or "
            Next
            xPath &= ")]"
            strUnion = " | "
        End If

        If CheckedListBox2.CheckedItems.Count = 0 Then
        ElseIf Not CheckedListBox2.GetItemChecked(0) Then
            ' /nitf/head/tobject/tobject.property[@tobject.property.type="Nyheter")]
            queryCount += 1
            xPath &= strUnion
            xPath &= "/nitf/head/tobject/tobject.property[("
            strOr = ""
            For Each itm In CheckedListBox2.CheckedItems
                xPath &= strOr & "@tobject.property.type='" & itm & "'"
                strOr = " or "
            Next
            xPath &= ")]"
            strUnion = " | "
        End If

        If CheckedListBox3.CheckedItems.Count = 0 Then
        ElseIf Not CheckedListBox3.GetItemChecked(0) Then
            '/nitf/head/tobject/tobject.subject[@tobject.subject.type="Sport"]
            queryCount += 1
            xPath &= strUnion
            xPath &= "/nitf/head/tobject/tobject.subject[("
            strOr = ""
            For Each itm In CheckedListBox3.CheckedItems
                xPath &= strOr & "@tobject.subject.type='" & itm & "'"
                strOr = " or "
            Next
            xPath &= ")]"
            strUnion = " | "
        End If

        If CheckedListBox4.CheckedItems.Count = 0 Then
        ElseIf Not CheckedListBox4.GetItemChecked(0) Then
            '/nitf/head/tobject/tobject.subject[@tobject.subject.matter='fotball']
            queryCount += 1
            xPath &= strUnion
            xPath &= "/nitf/head/tobject/tobject.subject[("
            strOr = ""
            For Each itm In CheckedListBox4.CheckedItems
                If Not itm.StartsWith("---") Then
                    xPath &= strOr & "@tobject.subject.matter='" & itm & "'"
                    strOr = " or "
                End If
            Next
            xPath &= ")]"
            strUnion = " | "
        End If

        If CheckedListBox5.CheckedItems.Count = 0 Then
        ElseIf Not CheckedListBox5.GetItemChecked(0) Then
            '/nitf/head/docdata/evloc[@state-prov='Norge']
            queryCount += 1
            xPath &= strUnion
            xPath &= "/nitf/head/docdata/evloc[("
            strOr = ""
            For Each itm In CheckedListBox5.CheckedItems
                xPath &= strOr & "@state-prov='" & itm & "'"
                strOr = " or "
            Next
            xPath &= ")]"
            strUnion = " | "
        End If

        If CheckedListBox6.CheckedItems.Count = 0 Then
        ElseIf Not CheckedListBox6.GetItemChecked(0) Then
            '/nitf/head/docdata/evloc[@county-dist='Oslo']
            queryCount += 1
            xPath &= strUnion
            xPath &= "/nitf/head/docdata/evloc[("
            strOr = ""
            For Each itm In CheckedListBox6.CheckedItems
                xPath &= strOr & "@county-dist='" & itm & "'"
                strOr = " or "
            Next
            xPath &= ")]"
            strUnion = " | "
        End If

        Me.TextBox2.AppendText(xPath & vbCrLf)
        Me.TextBox3.Text = queryCount.ToString
    End Sub

    Private Sub SetSubcats()
        Me.CheckedListBox4.Items.Clear()
        Me.CheckedListBox4.Items.Add("Alle")
        Dim itm As String
        For Each itm In Me.CheckedListBox3.CheckedItems
            Select Case itm
                Case "Kultur og underholdning"
                    Me.CheckedListBox4.Items.Add("--- KULTUR -------------------", True)
                    FillOneBox(CheckedListBox4, 4, 16)
                Case "Kuriosa og kjendiser"
                    Me.CheckedListBox4.Items.Add("--- KURIOSA -------------------", True)
                    FillOneBox(CheckedListBox4, 4, 32)
                Case "Sport"
                    Me.CheckedListBox4.Items.Add("--- SPORT ---------------------", True)
                    FillOneBox(CheckedListBox4, 4, 2048)
                Case "�konomi og n�ringsliv"
                    Me.CheckedListBox4.Items.Add("--- �KONOMI -------------------", True)
                    FillOneBox(CheckedListBox4, 4, 65536)
            End Select
        Next
    End Sub

    Private Sub SetProducts()
        ClearCheckbox(CheckedListBox1)
        ClearCheckbox(CheckedListBox2)
        ClearCheckbox(CheckedListBox3)
        ClearCheckbox(CheckedListBox7)

        CheckedListBox2.Enabled = True
        CheckedListBox3.Enabled = True
        CheckedListBox4.Enabled = True
        CheckedListBox5.Enabled = True
        CheckedListBox6.Enabled = True
        Dim setEnable As Boolean = True

        Dim itm As String
        For Each itm In Me.CheckedListBox9.CheckedItems
            Select Case itm
                Case "Nyhetstjenesten"
                    CheckedListBox1.SetItemChecked(CheckedListBox1.FindStringExact("Innenriks"), True)
                    CheckedListBox1.SetItemChecked(CheckedListBox1.FindStringExact("Utenriks"), True)
                    CheckedListBox1.SetItemChecked(CheckedListBox1.FindStringExact("Sport"), True)
                    CheckedListBox1.SetItemChecked(CheckedListBox1.FindStringExact("Priv-til-red"), True)
                    CheckedListBox7.SetItemChecked(CheckedListBox7.FindStringExact("Ut-Satellitt"), True)
                Case "NTB Direkte"
                    CheckedListBox1.SetItemChecked(CheckedListBox1.FindStringExact("Innenriks"), True)
                    CheckedListBox1.SetItemChecked(CheckedListBox1.FindStringExact("Utenriks"), True)
                    CheckedListBox1.SetItemChecked(CheckedListBox1.FindStringExact("Sport"), True)
                    CheckedListBox7.SetItemChecked(CheckedListBox7.FindStringExact("Ut-Direkte"), True)

                Case "Kultur og Underholdning"
                    CheckedListBox1.SetItemChecked(CheckedListBox1.FindStringExact("Kultur og Underholdning"), True)
                    CheckedListBox3.SetItemChecked(CheckedListBox3.FindStringExact("Kultur og Underholdning"), True)
                    CheckedListBox7.SetItemChecked(CheckedListBox7.FindStringExact("Ut-Satellitt"), True)
                    CheckedListBox7.SetItemChecked(CheckedListBox7.FindStringExact("Ut-Kultur"), True)
                Case "Kultur Direkte"
                    'CheckedListBox1.SetItemChecked(CheckedListBox1.FindStringExact("Innenriks"), True)
                    CheckedListBox7.SetItemChecked(CheckedListBox7.FindStringExact("Ut-KulturDirekte"), True)
                Case "Feature"
                    CheckedListBox1.SetItemChecked(CheckedListBox1.FindStringExact("Helse"), True)
                    CheckedListBox1.SetItemChecked(CheckedListBox1.FindStringExact("Reiser"), True)
                    CheckedListBox1.SetItemChecked(CheckedListBox1.FindStringExact("Moter/Trender"), True)
                    CheckedListBox1.SetItemChecked(CheckedListBox1.FindStringExact("PC/Internett"), True)
                    CheckedListBox1.SetItemChecked(CheckedListBox1.FindStringExact("Forbruker"), True)
                    CheckedListBox2.SetItemChecked(CheckedListBox2.FindStringExact("Feature"), True)
                    CheckedListBox7.SetItemChecked(CheckedListBox7.FindStringExact("Ut-Feature"), True)
                Case "Varsling/SMS"
                    'CheckedListBox1.SetItemChecked(CheckedListBox1.FindStringExact("Innenriks"), True)
                    'CheckedListBox7.SetItemChecked(CheckedListBox7.FindStringExact("Ut-Direkte"), True)
                Case "Lyd-nyheter"
                    'CheckedListBox1.SetItemChecked(CheckedListBox1.FindStringExact("Innenriks"), True)
                    'CheckedListBox7.SetItemChecked(CheckedListBox7.FindStringExact("Ut-Direkte"), True)

                    setEnable = False
                Case "Tips"
                    CheckedListBox1.SetItemChecked(CheckedListBox1.FindStringExact("Sport"), True)
                    CheckedListBox2.SetItemChecked(CheckedListBox2.FindStringExact("Tabeller og resultater"), True)
                    CheckedListBox3.SetItemChecked(CheckedListBox3.FindStringExact("Sport"), True)
                    CheckedListBox7.SetItemChecked(CheckedListBox7.FindStringExact("Ut-Satellitt"), True)
                    setEnable = False
                Case "Personalia"
                    CheckedListBox1.SetItemChecked(CheckedListBox1.FindStringExact("Jubilant-omtaler"), True)
                    CheckedListBox2.SetItemChecked(CheckedListBox2.FindStringExact("Profiler og intervjuer"), True)
                    CheckedListBox2.SetItemChecked(CheckedListBox2.FindStringExact("Nekrologer"), True)
                    CheckedListBox7.SetItemChecked(CheckedListBox7.FindStringExact("Ut-Satellitt"), True)
                    setEnable = False
                Case "Dagen i Dag"
                    CheckedListBox1.SetItemChecked(CheckedListBox1.FindStringExact("Dagen i dag"), True)
                    CheckedListBox7.SetItemChecked(CheckedListBox7.FindStringExact("Ut-Satellitt"), True)
                    setEnable = False
                    setEnable = False
                Case "Pressemeldinger"
                    CheckedListBox1.SetItemChecked(CheckedListBox1.FindStringExact("Formidlingstjenester"), True)
                    CheckedListBox2.SetItemChecked(CheckedListBox2.FindStringExact("PRM-tjenesten"), True)
                    CheckedListBox7.SetItemChecked(CheckedListBox7.FindStringExact("Ut-Satellitt"), True)
                    setEnable = False

            End Select
        Next

        CheckedListBox2.Enabled = setEnable
        CheckedListBox3.Enabled = setEnable
        CheckedListBox4.Enabled = setEnable
        CheckedListBox5.Enabled = setEnable
        CheckedListBox6.Enabled = setEnable

    End Sub

    Private Sub ClearCheckbox(ByVal box As CheckedListBox)
        Dim i As Integer
        For i = 0 To box.Items.Count - 1
            box.SetItemChecked(i, False)
        Next

    End Sub

    Private Sub FillDataSets()
        Dim cn As New OleDbConnection(sqlConnectionString)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand = New OleDbCommand()
        'Try
        cn.Open()
        'Catch e As Exception
        'WriteErr(errFilePath, "Feil i OpenDatabase: cn.Open", e)
        'Return (False)
        'End Try

        command.Connection = cn
        dataAdapter.SelectCommand = command

        dsBitnames = New DataSet()

        FillOneDataSet(dsBitnames, "dsBitnames", "SELECT * FROM GetBitnames", dataAdapter, command)

        cn.Close()
        dataAdapter = Nothing
        command = Nothing
    End Sub

    Private Sub FillOneDataSet(ByRef dsDataSet As DataSet, ByVal strOfflineFile As String, ByVal strCommand As String, ByVal dataAdapter As OleDbDataAdapter, ByVal command As OleDbCommand, Optional ByVal strGroupListID As String = "")
        'Dim strFile As String = databaseOfflinePath & "\" & strOfflineFile & strGroupListID & ".xml"
        command.CommandText = strCommand
        'Try
        dataAdapter.Fill(dsDataSet)
        'dsDataSet.WriteXml(strFile)
        'Catch e As DataException
        'WriteErr(errFilePath, "Feil i FillOneDataSet: ", e)
        'dsDataSet.ReadXml(strFile)
        'End Try
    End Sub

    Private Sub FillListBoxes()

        FillOneBox(CheckedListBox1, 0)
        FillOneBox(CheckedListBox2, 1)
        FillOneBox(CheckedListBox3, 3)
        FillOneBox(CheckedListBox5, 5)
        FillOneBox(CheckedListBox6, 6)

    End Sub

    Private Sub FillOneBox(ByVal box As CheckedListBox, ByVal typeOfName As Integer, Optional ByVal category As Integer = 0)

        Dim dsView As New Data.DataView()
        dsView.Table = dsBitnames.Tables(0)
        dsView.RowFilter = "TypeOfName = " & typeOfName
        If category > 0 Then
            dsView.RowFilter &= " and Category = " & category
        Else
            box.Items.Add("Alle")
        End If

        Dim row As DataRow
        Dim i As Integer
        For i = 0 To dsView.Count - 1
            row = dsView.Item(i).Row
            box.Items.Add(row.Item("IptcVal"))
        Next

    End Sub


    Private Sub CheckedListBox9_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles CheckedListBox9.ItemCheck
        'SetProducts()
    End Sub

    Private Sub CheckedListBox9_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckedListBox9.SelectedValueChanged
        SetProducts()
    End Sub

End Class
